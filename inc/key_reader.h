/*=============================================================================
 * Author: David Broin <davidmbroin@gmail.com>
 * Date: 2020/04/21
 * Version: 0.0
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/
#ifndef __KEY_READER_H__
#define __KEY_READER_H__

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"

/*=====[C++ - begin]=========================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/
typedef struct {
	gpioMap_t key;
	bool_t pressed;
} key_status_t;

/*=====[Definitions of private data types]===================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/
void keyfsmInit();
void keyfsmUpdate();

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/
#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __KEY_READER_H__ */
