/*=============================================================================
 * Author: David Broin <davidmbroin@gmail.com>
 * Date: 2020/04/21
 * Version: 0.0
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/
#ifndef __SAS_H__
#define __KEYS_READER_H__

/*=====[Inclusions of public function dependencies]==========================*/
#include "sapi.h"
#include "key_reader.h"

/*=====[C++ - begin]=========================================================*/
#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/
void sasfsmInit();
void sasfsmUpdate(void* taskParam);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[C++ - end]===========================================================*/
#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __SAS_H__ */
