/*=============================================================================
 * Author: David Broin <davidmbroin@gmail.com>
 * Date: 2020/04/21
 * Version: 0.0
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/
#include "key_reader.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/
typedef enum {
	INIT, UP, FALLING, DOWN, RISING
} fsmState_t;

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
static fsmState_t keyfsmState;
static uint8_t contador;

/*=====[Prototypes (declarations) of private functions]======================*/
static void keyfsmError(void);

/*=====[Implementations of public functions]=================================*/
// FSM Initialize Function
void keyfsmInit() {
	keyfsmState = INIT;   // Set initial state
}

// FSM Update Sate Function
void keyfsmUpdate(void* taskParam) {
	gpioMap_t key = ((key_status_t*) taskParam)->key;
	switch (keyfsmState) {
	case INIT:
		keyfsmState = UP;
		break;
	case UP:
		((key_status_t*) taskParam)->pressed=FALSE;
		if (!gpioRead(key)) {
			keyfsmState = FALLING;
		}
		break;
	case FALLING:
		printf("Evitando rebote en bajada\r\n");
		if (contador > 1) {
			if (!gpioRead(key)) {
				keyfsmState = DOWN;
				contador = 0;
			} else {
				keyfsmState = UP;
			}
		}
		contador++;
		break;
	case DOWN:
		((key_status_t*) taskParam)->pressed=TRUE;
		if (gpioRead(key)) {
			keyfsmState = RISING;
		}
		break;
	case RISING:
		printf("Evitando rebote en subida\r\n");
		if (contador > 1) {
			if (gpioRead(key)) {
				keyfsmState = UP;
				contador = 0;
			} else {
				keyfsmState = DOWN;
			}
		}
		contador++;
		break;
	default:
		keyfsmError();
		break;
	}
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/
static void keyfsmError(void) {
	// Error handler, example, restart FSM:
	// fsmState = STATE_INIT;
}
