/*=============================================================================
 * Author: David Broin <davidmbroin@gmail.com>
 * Date: 2020/04/21
 * Version: 0.0
 *===========================================================================*/

/*=====[Definition macros of private constants]==============================*/

// The maximum number of tasks required at any one time during the execution
// of the program. MUST BE ADJUSTED FOR EACH NEW PROJECT
#define SCHEDULER_MAX_TASKS   (10)

/*=====[Inclusions of function dependencies]=================================*/
#include "sapi.h"
#include "seos_pont.h"
#include "sas.h"
#include "key_reader.h"

/*=====[Definitions of public global variables]==============================*/
static key_status_t keyStatus;

/*=====[Main function, program entry point after power on or reset]==========*/
int main(void) {
	boardInit();

	keyStatus.key = TEC1;
	keyStatus.pressed = FALSE;
	// Initialize myTask
	sasfsmInit(); // Parameter passed into the task init.
	keyfsmInit();

	// Initialize scheduler
	schedulerInit();

	// Se agrega la tarea tarea1 al planificador
	schedulerAddTask(sasfsmUpdate, // Function that implements the task update.
			(void*) &keyStatus,        // Parameter passed into the task update.
			0,            // Execution offset in ticks.
			20           // Periodicity of task execution in ticks.
			);

	// Se agrega la tarea tarea1 al planificador
	schedulerAddTask(keyfsmUpdate, // Function that implements the task update.
			(void*) &keyStatus,        // Parameter passed into the task update.
			0,            // Execution offset in ticks.
			20           // Periodicity of task execution in ticks.
			);

	// Initialize task scheduler each 1ms.
	schedulerStart(1);

	while (true) {
		// Dispatch (execute) tasks that are mark to be execute by scheduler.
		schedulerDispatchTasks();
	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
