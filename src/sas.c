/*=============================================================================
 * Author: David Broin <davidmbroin@gmail.com>
 * Date: 2020/04/21
 * Version: 0.0
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/
#include "sas.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/
typedef enum {
	INIT,
	CONNECT,
	CONNECT_ANS,
	GENERAL_POLL_REQ,
	GENERAL_POLL_ANS,
	GENERAL_POLL_ACK,
	LONG_POLL_REQ,
	LONG_POLL_ANS,
	WAIT
} fsmState_t;

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/
static fsmState_t sasfsmState;
static uint8_t contador;

/*=====[Prototypes (declarations) of private functions]======================*/
static void fsmError(void);

/*=====[Implementations of public functions]=================================*/
// FSM Initialize Function
void sasfsmInit() {
	sasfsmState = INIT;   // Set initial state
}

// FSM Update Sate Function
void sasfsmUpdate(void* taskParam) {
	bool_t keyPressed = ((key_status_t*) taskParam)->pressed;
	uint8_t receivedByte;
	uint8_t receivedArray[4];
	static uint8_t i = 0;
	switch (sasfsmState) {
	case INIT:
		sasfsmState = CONNECT;
		break;
	case CONNECT:
		uartWriteByte(UART_USB, 0x80);
		uartWriteByte(UART_USB, 0x81);
		sasfsmState = CONNECT_ANS;
		break;
	case CONNECT_ANS:
		if (uartReadByte(UART_USB, &receivedByte)) {
			sasfsmState = GENERAL_POLL_REQ;
		} else if (contador >= 100) {
			printf("Timeout de Connect\r\n");
			sasfsmState = CONNECT;
			contador = 0;
		} else {
			contador++;
			sasfsmState = CONNECT_ANS;
		}
		break;
	case GENERAL_POLL_REQ:
		if (keyPressed) {
			gpioToggle(LED2);
			sasfsmState = LONG_POLL_REQ;
		} else {
			uartWriteByte(UART_USB, 0x80);
			sasfsmState = GENERAL_POLL_ANS;
		}
		break;
	case GENERAL_POLL_ANS:
		if (uartReadByte(UART_USB, &receivedByte)) {
			sasfsmState = GENERAL_POLL_ACK;
		} else if (contador >= 100) {
			printf("Timeout de GP\r\n");
			sasfsmState = CONNECT;
			contador = 0;
		} else {
			contador++;
			sasfsmState = GENERAL_POLL_ANS;
		}
		break;
	case GENERAL_POLL_ACK:
		uartWriteByte(UART_USB, 0x81);
		sasfsmState = WAIT;
		break;
	case LONG_POLL_REQ:
		uartWriteByte(UART_USB, 0x01);
		uartWriteByte(UART_USB, 0x1C);
		sasfsmState = LONG_POLL_ANS;
		break;
	case LONG_POLL_ANS:
		if (uartReadByte(UART_USB, &receivedByte)) {
			receivedArray[i] = receivedByte;
			if (i <= sizeof(receivedArray)) {
				i++;
				sasfsmState = LONG_POLL_ANS;
			} else {
				printf("\r\nLlego la cadena completa\r\n");
				printf("%s\r\n", receivedArray);
			}
			sasfsmState = GENERAL_POLL_REQ;
		}
		if (contador >= 100) {
			printf("Timeout de LP\r\n");
			sasfsmState = WAIT;
			contador = 0;
		}
		contador++;
		break;
	case WAIT:
		if (contador >= 10) {
			printf("Fin de espera\r\n");
			gpioToggle(LED3);
			sasfsmState = GENERAL_POLL_REQ;
			contador = 0;
		} else {
			contador++;
			sasfsmState = WAIT;
		}
		break;
	default:
		fsmError();
		break;
	}
}

/*=====[Implementations of interrupt functions]==============================*/

/*=====[Implementations of private functions]================================*/
static void fsmError(void) {
	// Error handler, example, restart FSM:
	// fsmState = STATE_INIT;
}
